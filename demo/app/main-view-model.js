var Observable = require("data/observable").Observable;

var wikitude;

function getMessage(counter) {
    if (counter <= 0) {
        return "Hoorraaay! You unlocked the NativeScript clicker achievement!";
    } else {
        return counter + " taps left";
    }
}

function createViewModel(page) {
    wikitude = page.getViewById('wikitude');
    var viewModel = new Observable();
    viewModel.counter = 42;
    viewModel.message = getMessage(viewModel.counter);

    viewModel.onTap = function() {
        this.counter--;
        this.set("message", getMessage(this.counter));
    }

    setTimeout(function () {
        console.log('testing setLocation');
        wikitude.setLocation(-122, 122, 1000, 5);
    }, 5000);

    return viewModel;
}

exports.createViewModel = createViewModel;
import { clearCache } from 'tns-core-modules/file-system/file-name-resolver';
import { ImageSource } from 'tns-core-modules/image-source';
import { ContentView } from "tns-core-modules/ui/content-view";
//--------------------------
// wikitude typings file.
//--------------------------

export class Wikitude extends ContentView {
    /**
     * Runs Javascript in the AR World, useful for sharing Data between the AR World, and your App.
     * @param {string} javascript - a String Containing the Javascript to be called Within the AR World. 
     */
    runJavaScript(javascript: string): void;

    /**
     * loads the URL of the Experience provided, and shows in wikitude AR.
     * @param {string} url - URL of Wikitude Experience 
     * @param {string[]} features - Required Features. 
     */
    loadUrl(url: string, features: string[]): void;

    /**
     * Reloads the Current AR World
     */
    reload(): void;

    /**
     * Checks if Device or License Supports a feature or array of Features.
     * @param features {string|string[]} - the feature, or features you want to check if device supports.
     */
    hasFeature(features: string[]): boolean;
    hasFeature(feature : string): boolean; 

    /**
     * Starts the Wikitude Plugin, if not already running.
     */
    start() : void;

    /**
     * Stops the Wiktiude Plugin, if Running.
     */
    stop() : void;

    /**
     * Restarts the Wikitude Plugin.
     */
    restart() : void;

    /**
     * Reports if the Wikitude Plugin is currently Running.
     */
    isRunning(): boolean;

    /**
     * Takes a screenshot.
     * @param {boolean} captureEverything - toggles capturing just the camera view and content, or including the webview as well.
     * @returns {Promise<ImageSource>} 
     * @memberof Wikitude
     */
    captureScreen(captureEverything: boolean) : Promise<ImageSource>

    /**
     * Clears the Cache of the wikitude html drawables.
     * @memberof Wikitude
     */
    clearCache() : void;

    /**
     * toggles the camera's flash.
     * @returns {boolean} - whether the flash is enabled or not.
     * @memberof Wikitude
     */
    toggleFlash() : boolean;

    /**
     * sets the ARView's Location
     * @returns {void}
     * @memberof Wikitude
     */
    setLocation(latitude: number, longitude: number, altitude: number, accuracy: number);

    /**
     * Disables Wikitude Default Location Provider
     * @returns {void}
     * @memberof Wikitude
     */
    disableLocationProvider();


    /**
     * re-enables Wikitude Default Location Provider
     * @returns {void}
     * @memberof Wikitude
     */
    enableLocationProvider();


}

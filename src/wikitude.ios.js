/**********************************************************************************
 * (c) 2017, Nathanael Anderson.
 * Licensed under the MIT license.
 *
 * Version 1.0.3                                        nathan@master-technology.com
 **********************************************************************************/
'use strict';

/* global NSObject, WeakRef, __extends, UIView */

const ContentView = require('ui/content-view').ContentView;
const application = require('application');
const view = require('ui/core/view');
const fs = require("file-system");

const ArchitectViewDelegate = NSObject.extend({
    _owner: null,
    initWithOwner: function (owner) {
        const delegate = new ArchitectViewDelegate();
        delegate._owner = new WeakRef(owner);
        return delegate;
    },
    architectViewDidFinishLoadArchitectWorldNavigation: function (architectView, navigation) {
        console.log("didFinishLoading");
        const owner = this._owner.get();
        if (owner) {
            owner._loadNavigation(navigation, null);
        }
    },
    architectViewDidFailToLoadArchitectWorldNavigationWithError: function (architectView, navigation, error) {
        console.log("Did fail loading", navigation, error);
        const owner = this._owner.get();
        if (owner) {
            owner._failNavigation(navigation, error);
        }
    },
    architectViewInvokedURL: function (architectView, url) {
        const owner = this._owner.get();
        if (owner) {
            owner._onUrlInvoked(url);
        }
    },
    architectViewDidEncounterInternalWarning: function (architectView, warning) {
        console.log("Encounter Warning", warning);
        const owner = this._owner.get();
        if (owner) {
            owner._onInternalWarning(warning);
        }
    },
    architectViewDidEncounterInternalError: function (architectView, error) {
        console.log("Encounter error", error);
        const owner = this._owner.get();
        if (owner) {
            owner._onInternalError(error);
        }
    },
    architectViewReceivedJSONObject: function (architectView, data) {
        console.log("Encounter data", data);

        function dictionaryToJson(nativeData) {
            const result = {};

            console.log(`Type Of nativeData ${typeof nativeData}`);

            if( nativeData instanceof NSDictionary ) {
                for(let i = 0, n = nativeData.allKeys.count; i < n; i++) {
                    let key = nativeData.allKeys.objectAtIndex(i);
                    let item =  nativeData.objectForKey(key);

                    if( item instanceof NSDictionary ) {
                        result[key] = dictionaryToJson(item);
                    } else {
                        result[key] = item;
                    }
                }
            } 

            return result;
        }


        const owner = this._owner.get();
        if (owner) {
            owner._onJavascriptCall(dictionaryToJson(data));
        }
    }
}, {
        protocols: [WTArchitectViewDelegate, WTArchitectViewDebugDelegate ]
    });

exports.licenseProperty = new view.Property({ name: "license" });
exports.urlProperty = new view.Property({ name: "url" });
class Wikitude extends ContentView {
    constructor() {
        super(arguments);
        this._delegate = null;
        this._motionManager = null;
        this._ios = null;
        this._licenseKey = global.wikitudeLicense || '';
        this._boundStart = this.start.bind(this);
        this._boundStop = this.stop.bind(this);
        this._dummy = false;
        this._url = null;
        this._features = null;
        this._loaded = false;
        this._licensed = false;
    }
    _createDummy() {
        this._dummy = true;
        this.backgroundColor = "#ff00ff";
        this._ios = UIView.alloc().init();
        this.start = this.stop = this.runJavaScript = this._boundStart = this._boundStop = function () { };
    }
    createNativeView() {
        console.log(`createNativeView`);
        this._loaded = false;
        this._licensed = false;
        if (!this.hasFeature(WTFeature_ImageTracking)) {
            this._createDummy();
            return;
        }
        this._delegate = ArchitectViewDelegate.alloc().initWithOwner(this);
        this._motionManager = new CMMotionManager();
        this._ios = new WTArchitectView(CGRectZero, this._motionManager);
        this._ios.delegate = this._delegate;
        this._ios.debugDelegate = this._delegate;
        if (this._licenseKey) {
            //console.log("Set Key", this._licenseKey);
            this._ios.setLicenseKey(this._licenseKey);
            this._licensed = true;
        }
        return this._ios;
    }
    initNativeView() {
        console.log(`initNativeView`);
        this.start();
        application.on(application.suspendEvent, this._boundStop);
        application.on(application.resumeEvent, this._boundStart);
        if (this._url && !this._loaded) {
            this.loadUrl(this.url, this._features);
        }
    }
    disposeNativeView() {
        console.log(`disposeNativeView`);
        this.stop();
        application.off(application.suspendEvent, this._boundStop);
        application.off(application.resumeEvent, this._boundStart);
    }
    _loadNavigation(nav) {
        console.log("Load Nav", nav);
        console.log("calling this.notify('loadedNavigation', owner):", this.url);
        this.notify({
            eventName: "loadedNavigation",
            object: this,
            data: { owner: this, navigation: nav, url: this.url }
        });
    }
    _failNavigation(nav, error) {
        this.notify({
            eventName: "failedNavigation",
            object: this,
            data: { owner: this, navigation: nav, error: error }
        });
    }
    _onUrlInvoked(url) {
        console.log("Url", url);
        this.notify({
            eventName: "urlInvoked",
            object: this,
            data: { owner: this, url: url }
        });
    }
    _onInternalWarning(warning) {
        console.log("Warning", warning);
        this.notify({
            eventName: "internalWarning",
            object: this,
            data: { owner: this, warning: warning }
        });
    }
    _onInternalError(error) {
        console.log("Warning", error);
        this.notify({
            eventName: "internalError",
            object: this,
            data: { owner: this, warning: error }
        });
    }
    _onJavascriptCall(data) {
        console.log("JSCall", data);
        this.notify({
            eventName: "javascript",
            object: this,
            data: { owner: this, data: data }
        });
    }
    hasFeature(feature) {
        const error = new interop.Reference();
        return !!WTArchitectView.isDeviceSupportedForRequiredFeaturesError(feature, error);
    }
    loadUrl(urlString, features) {
        this._loaded = false;
        this._url = urlString;
        this._features = features;
        if (!this._ios || this._dummy) {
            return;
        }
        let url;
        if (urlString.indexOf("~/") === 0) {
            urlString = urlString.replace("~/", fs.knownFolders.currentApp().path + "/");
            url = NSURL.fileURLWithPath(urlString.trim());
        }
        else {
            url = NSURL.URLWithString(urlString.trim());
        }
        console.log("LoadURL", urlString);
        if (typeof features === "undefined") {
            features = WTFeature_ImageTracking;
        }
        this._ios.loadArchitectWorldFromURLWithRequiredFeatures(url, features);
        this._loaded = true;
    }
    runJavaScript(javaScript) {
        // iOS uses a upper "S" in callJavaScript
        this._ios.callJavaScript(javaScript);
    }
    reload() {
        if (!this._url) {
            throw new Error("URL is null");
        }
        this.loadUrl(this._url, this._features);
    }
    // just reload current loaded world
    reloadWorld() {
        this._ios.reloadArchitectWorld();
    }
    onUnloaded() {
        ContentView.prototype.onUnloaded.call(this);
        this.stop();
    }
    start() {
        console.log('starting wikitude');

        if (this.isRunning()) {
            return;
        }
        this._ios.startCompletion(function (config) {
        }, function (isRunning, error) {
        });
    }
    stop() {
        console.log('stopping wikitude')
        if (!this.isRunning()) {
            return;
        }
        this._ios.stop();
    }
    setLocation(latitude, longitude, altitude, accuracy) {
        if (!this._ios) {
            return;
        }
        if (this._ios.injectLocationWithLatitudeLongitudeAltitudeAccuracy) {
            this._ios.injectLocationWithLatitudeLongitudeAltitudeAccuracy(latitude, longitude, altitude, accuracy);
        }
        else {
            this.logError();
        }
    }

    logError(message) {
        console.log(message || "Wikitude SDK doesn't appear to be accessible. It only works on a real device.");
    }

    isRunning() {
        if (this.ios) {
            console.log('_ios is truthy')

            if (!!this.ios.isRunning) {
                console.log('ios.isRunning = true')

                return true
            }
        }

        return false;
    }

    restart() {
        console.log("Restarting the Wikitude Plugin");

        if (this.isRunning()) {
            this.stop();
        }

        setTimeout(() => this.start(), 0);
    }

    /**
     * Disables the Location Provider
     */
    disableLocationProvider() {
        console.log('Disabling the Default Location Provider! Make sure you set the Location yourself');
        if( this.isRunning() ) {
            this.ios.setUseInjectedLocation(true);
        }
    }

    enableLocationProvider() {
        console.log('Enabling the default Location Provider');
        if( this.isRunning() && this.ios.isUsingInjectedLocation ) {
            this.ios.setUseInjectedLocation(false);
        }
    }
}
// __extends(Wikitude, ContentView);

Wikitude.prototype[exports.licenseProperty.getDefault] = function () {
    return "";
};
Wikitude.prototype[exports.licenseProperty.setNative] = function (license) {
    this._licenseKey = license;
    if (this._dummy) { return; }
    if (!this._licensed && this._ios) {
        this._ios.setLicenseKey(this._licenseKey);
        this._licensed = true;
    }
};
exports.licenseProperty.register(Wikitude);


Wikitude.prototype[exports.urlProperty.getDefault] = function () {
    return "";
};
Wikitude.prototype[exports.urlProperty.setNative] = function (val) {
    this.loadUrl(val);
};
exports.urlProperty.register(Wikitude);



















//module.exports = Wikitude;
exports.Wikitude = Wikitude;


/**********************************************************************************
 * (c) 2017, Nathanael Anderson.
 * Licensed under the MIT license.
 *
 * Version 1.1.4                                        nathan@master-technology.com
 **********************************************************************************/
'use strict';

/* global com, org, android, __extends, WeakRef, java */

const ContentView = require('tns-core-modules/ui/content-view').ContentView;
const application = require('tns-core-modules/application');
const view = require('tns-core-modules/ui/core/view');
const fs = require("tns-core-modules/file-system");
const imagesrc = require('tns-core-modules/image-source');


function WikitudePersistentDelegate(owner) {
    //noinspection JSUnusedGlobalSymbols
    let delegate = {
        // interfaces: [com.wikitude.architect.services.camera.CameraLifecycleListener, com.wikitude.architect.ArchitectView.ArchitectUrlListener, com.wikitude.architect.ArchitectView.ArchitectWorldLoadedListener, com.wikitude.architect.ArchitectJavaScriptInterfaceListener],
        bind: function (owner) {
            this._owner = new WeakRef(owner);
        },
        urlWasInvoked: function (url) {
            const owner = this._owner.get();
            if (owner) {
                owner._onUrlInvoked(url);
            }
            return true;
        },
        worldLoadFailed: function (errorCode, description, failingUrl) {
            console.log("World Load Failed", errorCode, description);
            const owner = this._owner.get();
            if (owner) {
                owner._failNavigation(failingUrl, errorCode);
            }
        },
        worldWasLoaded: function (url) {
            console.log("Was Loaded", url);
            const owner = this._owner.get();
            if (owner) {
                owner._loadNavigation(url);
            }
        },
        onJSONObjectReceived: function (data) {

            const owner = this._owner.get();
            if (owner) {
                owner._onJavascriptCall(data);
            }
        },
        onCameraOpen: function () {
            console.log("Camera is opened.");
            const owner = this._owner.get();
            if (owner) {
                owner._onCameraOpen();
            }
        },
        onCameraReleased: function () {
            console.log("Camera is released");
            // const owner = this._owner.get();
            // if (owner) {
            //     owner._onCameraReleased();
            // }
        },
        onCameraOpenAbort: function () {
            console.log("Camera aborted");
            const owner = this._owner.get();
            if (owner) {
                owner._onCameraAborted();
            }

        }
    };
    delegate.bind(owner);
    return delegate;
}

function WikitudeLocationProvider(owner, time, distance) {
    this._currentLocation = null;
    const context = getContext();
    this.locationManager = context.getSystemService(android.content.Context.LOCATION_SERVICE);
    if (time === 0) {
        this._time = 0;
    } else {
        this._time = time || 1000;
    }
    if (distance === 0) {
        this._distance = 0;
    } else {
        this._distance = distance || 1000;
    }
    this._running = false;
    const self = this;
    //noinspection JSUnusedGlobalSymbols
    this.listener = {
        onLocationChanged: function (location) {
            if (!self.isBetterLocation(location)) {
                return;
            }
            if (owner) {
                owner._onLocationChanged(location);
            }
        },

        onProviderDisabled: (provider) => {},

        onProviderEnabled: (provider) => {},

        onStatusChanged: (arg1, arg2, arg3) => {}
    };
    ///this.setOwner(owner);
    this.locationListener = new android.location.LocationListener(this.listener);
}

Object.defineProperty(WikitudeLocationProvider, "time", {
    get: function () {
        return this._time;
    },
    set: function (time) {
        if (this._time === time) {
            return;
        }
        this._time = time;
        if (this._running) {
            this._restart();
        }
    }
});
Object.defineProperty(WikitudeLocationProvider, "distance", {
    get: function () {
        return this._distance;
    },
    set: function (distance) {
        if (this._distance === distance) {
            return;
        }
        this._distance = distance;
        if (this._running) {
            this._restart();
        }
    }
});

WikitudeLocationProvider.prototype.isBetterLocation = function (location) {
    if (this._currentLocation === null) {
        return true;
    }
    const timeDelta = location.getTime() - this._currentLocation.getTime();
    const isSignificantlyNewer = timeDelta > 60000;
    const isSignificantlyOlder = timeDelta < -60000;
    const isNewer = timeDelta > 0;
    if (isSignificantlyNewer) {
        return true;
    } else if (isSignificantlyOlder) {
        return false;
    }

    // Check whether the new location fix is more or less accurate
    const accuracyDelta = location.getAccuracy() - this._currentLocation.getAccuracy();
    const isMoreAccurate = accuracyDelta < 0;
    if (isMoreAccurate) {
        return true;
    }

    const isLessAccurate = accuracyDelta > 0;
    if (isNewer && !isLessAccurate) {
        return true;
    }


    const isSignificantlyLessAccurate = accuracyDelta > 200;
    const isFromSameProvider = this.isSameProvider(location.getProvider(), this._currentLocation.getProvider());
    return isNewer && isFromSameProvider && !isSignificantlyLessAccurate;


};

WikitudeLocationProvider.prototype.isSameProvider = function (curLocation, oldLocation) {
    if (curLocation === null) {
        return oldLocation === null;
    }
    return curLocation.equals(oldLocation);
};

WikitudeLocationProvider.prototype.setOwner = function (owner) {
    this.listener._owner = new WeakRef(owner);
};

WikitudeLocationProvider.prototype._restart = function () {
    if (!this._running) {
        return;
    }
    this.pause();
    this.resume();
};

WikitudeLocationProvider.prototype.pause = function () {
    if (!this._running) {
        return;
    }
    this._running = false;

    this.locationManager.removeUpdates(this.locationListener);
};

WikitudeLocationProvider.prototype.resume = function () {
    if (this._running) {
        return;
    }
    this._running = true;

    this._setupEventListener(android.location.LocationManager.GPS_PROVIDER);
    this._setupEventListener(android.location.LocationManager.NETWORK_PROVIDER);
};

WikitudeLocationProvider.prototype._setupEventListener = function (provider) {
    // If the provider is not active, we don't do anything with it...
    if (!this.locationManager.isProviderEnabled(provider)) {
        return;
    }

    // Try to get the last known position of this provider and then send it on as if the listener got it normally
    let lastKnownPostion = this.locationManager.getLastKnownLocation(provider);
    if (lastKnownPostion !== null) {
        this.locationListener.onLocationChanged(lastKnownPostion);
    }

    // Setup the listener to receive all future events from this provider
    if (this.locationManager.getProvider(provider) !== null) {
        this.locationManager.requestLocationUpdates(provider, this._time, this._distance, this.locationListener);
    }
};



exports.licenseProperty = new view.Property({
    name: "license"
});
exports.urlProperty = new view.Property({
    name: "url"
});

class Wikitude extends ContentView {
    constructor() {
        super(arguments);
        this._android = null;
        this._licenseKey = global.wikitudeLicense || '';
        this._url = null;
        this._features = null;
        this._loaded = false;
        this._licensed = false;
        this._config = false;
        this._dummy = false;
        this._started = false;
        this._location = null;
        this._boundStart = this.start.bind(this);
        this._boundStop = this.stop.bind(this);
        this._flashEnabled = false;
    }
    _createDummy() {
        this._dummy = true;
        this.backgroundColor = "#ff00ff";
        this._android = new org.nativescript.widgets.StackLayout(this._context);
        this.start = this.stop = this.runJavaScript = this._boundStart = this._boundStop = function () {};
    }
    createNativeView() {
        console.log('createNativeView');
        this._loaded = false;
        this._licensed = false;

        const deviceNotSupported = this.hasFeature(null);

        if (!deviceNotSupported) {
            this._createDummy();
            return;
        }
        
        this._android = new com.wikitude.architect.ArchitectView(getCurrentActivity());
        this._config = new com.wikitude.architect.ArchitectStartupConfiguration();
        this._config.setFeatures(com.wikitude.architect.ArchitectStartupConfiguration.Features.Geo | com.wikitude.architect.ArchitectStartupConfiguration.Features.ImageTracking); // jshint ignore:line
        if (this._licenseKey) {
            this._config.setLicenseKey(this._licenseKey);
            this._licensed = true;
        }
        this._delegate = new WikitudePersistentDelegate(this);
        this._android.registerUrlListener(new com.wikitude.architect.ArchitectView.ArchitectUrlListener(this._delegate));
        this._android.registerWorldLoadedListener(new com.wikitude.architect.ArchitectView.ArchitectWorldLoadedListener(this._delegate));
        this._android.addArchitectJavaScriptInterfaceListener(new com.wikitude.architect.ArchitectJavaScriptInterfaceListener(this._delegate));
        // This event set crashes NativeScript currently
        // this._android.setCameraLifecycleListener(new com.wikitude.architect.services.camera.CameraLifecycleListener(this._delegate));
        
        this.location = new WikitudeLocationProvider(this, this._time, this._distance);

        return this._android;
    }
    initNativeView() {
        console.log('initNativeView');
            try {
                this._android.onCreate(this._config);
            } catch (e) {
                console.log("Wikitude Error:", e);
                this._createDummy();
                return;
            }
            this._android.onPostCreate();
        
        // Delay this until the UI is built
        setTimeout(() => {
            this.start();
        });
        application.on(application.suspendEvent, this._boundStop);
        application.on(application.resumeEvent, this._boundStart);
        if (this._url && !this._loaded) {
            this.loadUrl(this.url, this._features);
        }
    }
    disposeNativeView() {
        console.log('disposeNativeView');
        if (this._android) {
            this._android.onDestroy();
        }
        this.stop();
        application.off(application.suspendEvent, this._boundStop);
        application.off(application.resumeEvent, this._boundStart);
    }
    _onLocationChanged(location) {
        console.log('_onLocationChanged location:', location);
        console.log('_onLocationChanged this._android:', this._android);
        if (location && this._android) {
            console.log("Sending Location", location.getLongitude());
            if (location.hasAltitude() && location.hasAccuracy() && location.getAccuracy() < 7) {
                this._android.setLocation(location.getLatitude(), location.getLongitude(), location.getAltitude(), location.getAccuracy());
            } else {
                this._android.setLocation(location.getLatitude(), location.getLongitude(), location.hasAccuracy() ? location.getAccuracy() : 1000);
            }
        }
    }
    _loadNavigation(nav) {
        console.log("Load Nav", nav);
        this.notify({
            eventName: "loadedNavigation",
            object: this,
            data: {
                owner: this,
                navigation: nav,
                url: this.url
            }
        });
    }
    _failNavigation(nav, error) {
        this.notify({
            eventName: "failedNavigation",
            object: this,
            data: {
                owner: this,
                navigation: nav,
                error: error
            }
        });
    }
    _onUrlInvoked(url) {
        console.log("Url", url);
        this.notify({
            eventName: "urlInvoked",
            object: this,
            data: {
                owner: this,
                url: url
            }
        });
    }
    /* iOS Event -- Here so no errors occur */
    _onInternalWarning(warning) {
        console.log("Warning", warning);
        this.notify({
            eventName: "internalWarning",
            object: this,
            data: {
                owner: this,
                warning: warning
            }
        });
    }
    /* iOS Event -- Here so no errors occur */
    _onInternalError(error) {
        console.log("Warning", error);
        this.notify({
            eventName: "internalError",
            object: this,
            data: {
                owner: this,
                warning: error
            }
        });
    }
    _onJavascriptCall(data) {
        console.log("JSCall", data);
        this.notify({
            eventName: "javascript",
            object: this,
            data: {
                owner: this,
                data: JSON.parse(data)
            }
        });
    }
    _onCameraOpen() {
        console.log("Camera Open");
        this.notify({
            eventName: "cameraOpen",
            object: this,
            data: {
                owner: this
            }
        });
    }
    _onCameraClose() {
        console.log("Camera Close");
        this.notify({
            eventName: "cameraClose",
            object: this,
            data: {
                owner: this
            }
        });
    }
    _onCameraAbort() {
        console.log("Camera Abort");
        this.notify({
            eventName: "cameraAbort",
            object: this,
            data: {
                owner: this
            }
        });
    }
    hasFeature(feature) {
            if (feature === null) {
                return com.wikitude.architect.ArchitectView.isDeviceSupported(getCurrentActivity());
            }
    
            const mf = com.wikitude.architect.ArchitectView.isDeviceSupported(getCurrentActivity(), feature);
            return !mf.areFeaturesMissing();
    }
    loadUrl(urlString, features) {
        this._loaded = false;
        this._url = urlString;
        this._features = features;
        if (!this._android || this._dummy) {
            return;
        }
        if (urlString.indexOf("~/") === 0) {
            urlString = urlString.replace("~/", "file://" + fs.knownFolders.currentApp().path + "/");
        }
        console.log("LoadURL", urlString);
        this._android.load(urlString);
        this._loaded = true;
    }
    runJavaScript(javaScript) {
        // Android is lower case 's' in callJavascript
        this._android.callJavascript(javaScript);
    }
    reload() {
        if (!this._url) {
            throw new Error("URL is null");
        }
        this.loadUrl(this._url, this._features);
    }
    onUnloaded() {
        ContentView.prototype.onUnloaded.call(this);
        this.stop();
    }
    start() {
        console.log("Starting Wikitude Plugin", Date.now());
        if (!this._started) {
            this._started = true;
            this._android.onResume();
        }
        if (this.location) {
            this.location.resume();
        }
    }
    stop() {
        console.log("Stopping Wikitude Plugin", Date.now());
        if (this.location) {
            this.location.pause();
        }
        if (this._started) {
            this._android.onPause();
            this._started = false;
        }
        if (!this.isRunning()) {
            return;
        }
    }

    clearCache() {
        this._android.clearCache();
    }

    toggleFlash() {
        this._flashEnabled = this._flashEnabled ? false : true;
        this._android.setFlashEnabled(this._flashEnabled);
        return this._flashEnabled;
    }

    captureScreen(captureEverything = false) {
        const captureMode = captureEverything ? ArchitectView.CaptureScreenCallback.CAPTURE_MODE_CAM_AND_WEBVIEW : ArchitectView.CaptureScreenCallback.CAPTURE_MODE_CAM;
        
        try {
            return new Promise((resolve, reject) => {
                const cb = new ArchitectView.CaptureScreenCallback();
                cb.onScreenCaptured((image) => resolve(imagesrc.fromNativeSource(image)));

                this._android.captureScreen(captureMode, cb);
            });
        } catch(e) {
            return Promise.reject(e);
        } 
    }
    isRunning() {
        try {
            if (!!this._started) {
                if (!!this.android) {
                    if (!!this._android.isRunning || this._delegate.isRunning) {
                        return true
                    }
                }
            }
            return false;
        } catch (e) {
            console.log(e.message);
            return false;
        }
    }
    setLocation(latitude, longitude, altitude, accuracy) {
        if (!this.nativeView) {
            return;
        }
        this._android.setLocation(latitude, longitude, altitude, accuracy);
    }
    restart() {
        console.log("restarting Wiktiude");
        if (this.isRunning()) {
            this.stop();
        }
        setTimeout(() => this.start(), 0);
    }

    disableLocationProvider() {
        console.log('Disabling the Default Location Provider! Make sure you set the Location yourself');
        if( this.isRunning() ) {
            this.location.pause();
        }
    }

    enableLocationProvider() {
        console.log('Enabling the default Location Provider');
        if( this.isRunning() ) {
            this.location.resume();
        }
    }
}

Wikitude.prototype[exports.licenseProperty.getDefault] = function () {
    return "";
};
Wikitude.prototype[exports.licenseProperty.setNative] = function (license) {
    this._licenseKey = license;
    if (this._dummy) {
        return;
    }
    if (!this._licensed && this._android) {
        this._config.setLicenseKey(this._licenseKey);
        this._licensed = true;
    }
};
exports.licenseProperty.register(Wikitude);


Wikitude.prototype[exports.urlProperty.getDefault] = function () {
    return "";
};
Wikitude.prototype[exports.urlProperty.setNative] = function (val) {
    this.loadUrl(val);
};
exports.urlProperty.register(Wikitude);


function getContext() {
    const application = require('application');

    if (application.android.context) {
        return (application.android.context);
    }
    let ctx = java.lang.Class.forName("android.app.AppGlobals").getMethod("getInitialApplication", null).invoke(null, null);
    if (ctx) {
        return ctx;
    }

    ctx = java.lang.Class.forName("android.app.ActivityThread").getMethod("currentApplication", null).invoke(null, null);
    return ctx;
}

function getCurrentActivity() {
    const application = require('tns-core-modules/application');
    return application.android.foregroundActivity || application.android.startActivity
}

//module.exports = Wikitude;
exports.Wikitude = Wikitude;
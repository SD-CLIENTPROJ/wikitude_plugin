# @spartadigital/nativescript-ns-wikitude 

This is under active development. Please Feel free to Contribute and submit any pull requests or issues.

Uses Wikitude version 7.1.

## License
This plugin is licensed under the MIT license

a license for Wikitude can be obtained from them, either a trial or paid license will work.

[You can obtain a trial license from Here](http://www.wikitude.com/developer/licenses)

## Installation
to install just run 
```(terminal)
    $ npm i --save @spartadigital/nativescript-ns-wikitude
```

You then need to link your license key, you can do this by adding the following in your main.ts/main.js:

```(javascript)
global.wikitudeLicense = "[YOUR-WIKITUDE-LICENSE-HERE]"
```


For {A} in your `app.component.ts` you should include : 

```(typescript/javascript)
import { registerElement } from 'nativescript-angular/element-registry';
import { Wikitude } from '@spartadigital/nativescript-ns-wikitude';

registerElement('Wikitude', () => Wikitude);
``` 

## Usage & API

Coming Soon™...

## Example

Example Using Wikitude plugin in {A} :

wikitude-component.html
```(html)
<AbsoluteLayout class="w-full h-full dashboard">

    <Wikitude #wikitude
            top="0"
            left="0"
            [url]="worldUrl"
            class="wikitude w-full h-full"    
            (loadedNavigation)="onLoaded($event)"          
            (javascript)="onJSON($event)"
            (internalError)="onError($event)">
    </Wikitude>

</AbsoluteLayout>
```

wikitude-component.ts
```(typescript)
export class WikitudeComponent {
    ...*snip*...
    // using ~/ means that the file is a local file.
    public worldUrl: string = "~/wk_world/index.html"

    onError($event) {
        console.log(`Wikitude Error : ${$event.errorObject.message}`);
    }

    onJSON($event) {
        const json = <{ event: string; message: string }>JSON.parse($event.data.data);

        alert(json.message);
    }
   
    ...*snip*...
}


```
## Contributing

Contribution Guidelines are coming soon!